﻿unit uLanguageAnalyzer;

interface

uses
  RegularExpressions,
  StrUtils,
  SysUtils,
  uElementsList,
  Math;

type
  TMetrics = record
    GeneralOperatorsCount: Integer;
    GeneralOperandsCount: Integer;
    UniqueOperatorsCount: Integer;
    UniqueOperandsCount: Integer;
    OperatorsList: TElementsList;
    OperandsList: TElementsList;
    ProgramDictionary: Integer;
    ProgramLength: Integer;
    ProgramVolume: Integer;
  end;

  TLanguageAnalyzer = class
  private
    FProgramCode: string;
    FMetrics: TMetrics;
    function GetMetrics(): TMetrics;
    procedure DeleteSingleLineComments();
    procedure DeleteMultiLineComments();
    procedure HandleLiterals();
    procedure DeleteVarConstDeclarations();
    procedure DeleteControlCharacters();
    procedure KeepFuncBodiesInCode();
    procedure DeleteFuncCalls();
    procedure DeleteKeyword(AKeyword: string; IsOperator: Boolean);
    procedure DeleteAllOperators();
    procedure DeleteAllKeywords();
    procedure HandleOperands();
    procedure FillMetrics();
  public
    procedure ParseProgram(AProgramCode: string);
    property Metrics: TMetrics read GetMetrics;
  end;

implementation

procedure TLanguageAnalyzer.ParseProgram(AProgramCode: string);
begin
  Self.FMetrics.OperandsList := TElementsList.Create();
  Self.FProgramCode := AProgramCode;

  Self.FMetrics.OperatorsList := TElementsList.Create();
  Self.FMetrics.OperandsList := TElementsList.Create();

  Self.DeleteSingleLineComments();
  Self.DeleteMultiLineComments();
  Self.DeleteVarConstDeclarations();
  Self.DeleteControlCharacters();
  Self.KeepFuncBodiesInCode();
  Self.HandleLiterals();
  Self.DeleteFuncCalls();
  Self.DeleteAllOperators();
  Self.DeleteAllKeywords();
  Self.HandleOperands();
  Self.FillMetrics();
end;

procedure TLanguageAnalyzer.DeleteSingleLineComments;
const
  Pattern = '\s*//.*'; // Однострочный комментарий и пробелы до него
begin
  Self.FProgramCode := TRegEx.Replace(Self.FProgramCode, Pattern, '');
end;

procedure TLanguageAnalyzer.DeleteMultiLineComments;
const
  // Многострочный комментарий и пробелы до него
  Pattern = '\s*/\*[^*]*\*+(?:[^/*][^*]*\*+)*/';
begin
  Self.FProgramCode := TRegEx.Replace(Self.FProgramCode, Pattern, '');
end;

procedure TLanguageAnalyzer.DeleteVarConstDeclarations;
  procedure DeleteDeclarations(APattern: string);
  var
    DeclarationMatch: TMatch;
  begin
    DeclarationMatch := TRegEx.Match(Self.FProgramCode, APattern);

    while DeclarationMatch.Success do
    begin
      Delete(Self.FProgramCode,
        DeclarationMatch.Index, DeclarationMatch.Length - 1);
      Self.FProgramCode[DeclarationMatch.Index] := ' ';
      DeclarationMatch := TRegEx.Match(Self.FProgramCode, APattern);
    end;
  end;

const
  VarConstSemicolonPattern = '(?:\n|\r|\t| )(?:var|const) .+?;';
  VarConstEndLinePattern = '(?:\n|\r|\t| )(?:var|const) .+';
begin
  DeleteDeclarations(VarConstSemicolonPattern);
  DeleteDeclarations(VarConstEndLinePattern);
end;

procedure TLanguageAnalyzer.DeleteControlCharacters;
begin
  Self.FProgramCode := ReplaceStr(Self.FProgramCode, #$09, ' ');
  Self.FProgramCode := ReplaceStr(Self.FProgramCode, #$0A, ' ');
  Self.FProgramCode := ReplaceStr(Self.FProgramCode, #$0D, ' ');
end;

procedure TLanguageAnalyzer.KeepFuncBodiesInCode;
  function CountFuncBodySize(): Integer;
  var
    CurlyBracesCount, CharIndex: Integer;
  begin
    CurlyBracesCount := 1;
    CharIndex := 2;
    while CurlyBracesCount <> 0 do
    begin
      if Self.FProgramCode[CharIndex] = '{' then
        Inc(CurlyBracesCount)
      else if Self.FProgramCode[CharIndex] = '}' then
        Dec(CurlyBracesCount);

      Inc(CharIndex);
    end;

    Result := CharIndex - 1;
  end;

const
  FuncPattern = '(func [^{]*)';
var
  FuncMatch: TMatch;
  CodeBuff: string;
  FuncBodySize: Integer;
begin
  { Алгоритм:
      1) Найти объявление функции
      2) Убрать из текста символы до тела найденой функции
      3) Посчитать размер тела функции
      4) Добавить тело функции в буфер
      5) Пока в тексте осталось объявление функции, повторять с шага 1
      6) Перезаписать текст значением из буфера
  }

  CodeBuff := '';
  FuncMatch := TRegEx.Match(Self.FProgramCode, FuncPattern);

  while FuncMatch.Success do
  begin
    Delete(Self.FProgramCode, 1, FuncMatch.Index + FuncMatch.Length - 1);
    FuncBodySize := CountFuncBodySize();
    CodeBuff := CodeBuff + Copy(Self.FProgramCode, 1, FuncBodySize);
    FuncMatch := TRegEx.Match(Self.FProgramCode, FuncPattern);
  end;

  Self.FProgramCode := CodeBuff;
end;

procedure TLanguageAnalyzer.DeleteFuncCalls;
  procedure DeleteClosingBracket(StartIndex: Integer);
  var
    BracketsCount, CharIndex: Integer;
  begin
    BracketsCount := 1;
    CharIndex := StartIndex;

    while BracketsCount <> 0 do
    begin
      if Self.FProgramCode[CharIndex] = '(' then
        Inc(BracketsCount)
      else if Self.FProgramCode[CharIndex] = ')' then
        Dec(BracketsCount);

      Inc(CharIndex);
    end;

    Self.FProgramCode[CharIndex - 1] := ' ';
  end;

const
  FuncCallPattern = '(\w+\()';
var
  FuncCallMatch: TMatch;
  FuncCallOperator: string;
begin
  { Алгоритм:
      1) Найти вызов функции
      2) Добавить идентификатор в список операторов
      3) Удалить идентификатор вызываемой функции и открывающую скобку
      4) Найти и удалить соответствующую закрывающую скобку
      5) Пока в тексте остался вызов функции, повторять с шага 1
  }

  FuncCallMatch := TRegEx.Match(Self.FProgramCode, FuncCallPattern);

  while FuncCallMatch.Success do
  begin
    FuncCallOperator := FuncCallMatch.Value + ' )';
    Self.FMetrics.OperatorsList.AddElement(FuncCallOperator);

    Delete(Self.FProgramCode, FuncCallMatch.Index, FuncCallMatch.Length - 1);
    Self.FProgramCode[FuncCallMatch.Index] := ' ';
    DeleteClosingBracket(FuncCallMatch.Index + 1);
    FuncCallMatch := TRegEx.Match(Self.FProgramCode, FuncCallPattern);
  end;
end;

procedure TLanguageAnalyzer.DeleteKeyword(AKeyword: string;
  IsOperator: Boolean);
var
  KeywordPattern: string;
  KeywordMatch: TMatch;
  KeywordPosition: Integer;
begin
  KeywordPattern := '(?:^| )' + AKeyword + '(?:$| )';

  KeywordMatch := TRegEx.Match(Self.FProgramCode, KeywordPattern);

  while KeywordMatch.Success do
  begin
    if IsOperator then
      Self.FMetrics.OperatorsList.AddElement(AKeyword);

    KeywordPosition := KeywordMatch.Index;
    Delete(Self.FProgramCode, KeywordPosition, Length(AKeyword) + 1);
    Insert('  ', Self.FProgramCode, KeywordPosition);

    KeywordMatch := TRegEx.Match(Self.FProgramCode, KeywordPattern);
  end;
end;

procedure TLanguageAnalyzer.DeleteAllOperators;
  procedure DeleteOperator(AOperator: string);
  begin
    while Pos(AOperator, Self.FProgramCode) <> 0 do
    begin
      Self.FMetrics.OperatorsList.AddElement(AOperator);
      Self.FProgramCode :=
        StringReplace(Self.FProgramCode, AOperator, ' ', []);
    end;
  end;

  procedure DeleteBrackets(AOpeningBracket, AClosingBracket: string);
  var
    OpeningBracketPos, ClosingBracketPos: Integer;
    OperatorName: string;
  begin
    OperatorName := AOpeningBracket + ' ' + AClosingBracket;
    OpeningBracketPos := Pos(AOpeningBracket, Self.FProgramCode);

    while OpeningBracketPos <> 0 do
    begin
      Self.FMetrics.OperatorsList.AddElement(OperatorName);

      Self.FProgramCode[OpeningBracketPos] := ' ';
      ClosingBracketPos := Pos(AClosingBracket, Self.FProgramCode);
      Self.FProgramCode[ClosingBracketPos] := ' ';

      OpeningBracketPos := Pos(AOpeningBracket, Self.FProgramCode);
    end;
  end;

  procedure DeleteDotOperator;
  const
    DotPattern = '[_a-zA-Z][_a-zA-Z0-9]*\.';
  var
    DotOperatorMatch: TMatch;
    MatchDotPosition, CodeDotPosition: Integer;
  begin
    DotOperatorMatch := TRegEx.Match(Self.FProgramCode, DotPattern);

    while DotOperatorMatch.Success do
    begin
      Self.FMetrics.OperatorsList.AddElement('.');

      MatchDotPosition := Pos('.', DotOperatorMatch.Value);
      CodeDotPosition := DotOperatorMatch.Index + MatchDotPosition - 1;
      Self.FProgramCode[CodeDotPosition] := ' ';

      DotOperatorMatch := TRegEx.Match(Self.FProgramCode, DotPattern);
    end;
  end;

const
  Operators: array [1..37] of string = ('<<=', '>>=', '&^=', '||', '&&',
                                        '==', '!=', '<=', '>=', '<<', '>>',
                                        '&^', '+=', '-=', '*=', '/=', '%=',
                                        '&=', '|=', '^=', '++', '--', '<-',
                                        ':=', '<', '>', '+', '-', '*', '/',
                                        '%', '^', '!', '|', '&', ';', '=');
  ControlOperators: array [1..7] of string = ('break', 'continue', 'return',
                                              'if', 'switch', 'for', 'goto');
var
  i: Integer;
begin
  for i := 1 to 37 do
    DeleteOperator(Operators[i]);

  DeleteBrackets('{', '}');
  DeleteBrackets('(', ')');
  DeleteDotOperator();

  for i := 1 to 7 do
    Self.DeleteKeyword(ControlOperators[i], True);
end;

function TLanguageAnalyzer.GetMetrics(): TMetrics;
begin
  Result := Self.FMetrics;
end;

procedure TLanguageAnalyzer.HandleLiterals();
  procedure FindAndReplaceLiterals(Pattern: string);
  var
    Literals: TMatchCollection;
    i: Integer;
  begin
    Literals := TRegEx.Matches(Self.FProgramCode, Pattern);
    for i := 1 to Literals.Count do
      Self.FMetrics.OperandsList.AddElement(Literals.Item[i-1].Value);
    Self.FProgramCode := TRegEx.Replace(Self.FProgramCode, Pattern, ' ');
  end;

const
  StringLiteralPattern = '"[^"]*"';
  SingleStringLiteralPattern = '`[^`]*`';
  RuneLiteralPattern = '''[^'']*''';
  HexExponentLiteralPattern = '\b0[xX][_]*[0-9a-fA-F]*\.?[0-9a-fA-F]*[pP][+-]?\d*i*';
  DecimalExponentLiteralPattern = '\b\d+\.\d*[eE][+-]?\d+i*';

begin
  FindAndReplaceLiterals(StringLiteralPattern);
  FindAndReplaceLiterals(SingleStringLiteralPattern);
  FindAndReplaceLiterals(RuneLiteralPattern);

  FindAndReplaceLiterals(HexExponentLiteralPattern);
  FindAndReplaceLiterals(DecimalExponentLiteralPattern);
end;

procedure TLanguageAnalyzer.DeleteAllKeywords;
const
  Keywords: array [1..15] of string = ('default', 'interface', 'select',
                                       'case', 'defer', 'go', 'map', 'else',
                                       'struct', 'chan', 'package', 'type',
                                       'fallthrough', 'range', 'import');
  IsOperator = False;
var
  i: Integer;
begin
  for i := 1 to 15 do
    Self.DeleteKeyword(Keywords[i], IsOperator);
end;

procedure TLanguageAnalyzer.HandleOperands;
  procedure FindAndReplaceOperands(Pattern: string);
  var
    Operands: TMatchCollection;
    i: Integer;
  begin
    Operands := TRegEx.Matches(Self.FProgramCode, Pattern);
    for i := 1 to Operands.Count do
    begin
      if (Trim(Operands.Item[i-1].Value) = '.') then
        Self.FMetrics.OperatorsList.AddElement(Operands.Item[i-1].Value)
      else
        Self.FMetrics.OperandsList.AddElement(Operands.Item[i-1].Value);
    end;
    Self.FProgramCode := TRegEx.Replace(Self.FProgramCode, Pattern, ' ');
  end;

const
  OperandPattern = '\s*([a-zA-Z0-9_.]+)\s*';
begin
  FindAndReplaceOperands(OperandPattern);
end;

procedure TLanguageAnalyzer.FillMetrics;
begin
  with Self.FMetrics do
  begin
    GeneralOperatorsCount := OperatorsList.Size;
    GeneralOperandsCount := OperandsList.Size;

    OperatorsList.MakeListUnique;
    OperandsList.MakeListUnique;

    UniqueOperatorsCount := OperatorsList.Size;
    UniqueOperandsCount := OperandsList.Size;

    ProgramDictionary := UniqueOperandsCount + UniqueOperatorsCount;
    ProgramLength := GeneralOperatorsCount + GeneralOperandsCount;
    if ProgramDictionary = 0 then
      ProgramVolume := 0
    else
      ProgramVolume := Round(ProgramLength * Log2(ProgramDictionary));
  end;
end;
end.
