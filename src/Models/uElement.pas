﻿unit uElement;

interface

type
  TElement = class
  private
    FName: string;
    FCount: Integer;
    FNextEl: TElement;
    FPrevEl: TElement;
    function GetName(): string;
    function GetCount(): Integer;
    function GetNextEl(): TElement;
    procedure SetNextEl(ANextEl: TElement);
    function GetPrevEl(): TElement;
    procedure SetPrevEl(APrevEl: TElement);
  public
    constructor Create(AName: string);
    procedure IncCount();
    property Name: string read GetName;
    property Count: Integer read GetCount;
    property NextElement: TElement read GetNextEl write SetNextEl;
    property PrevElement: TElement read GetPrevEl write SetPrevEl;
  end;

implementation

constructor TElement.Create(AName: string);
begin
  Self.FName := AName;
  Self.FCount := 1;
end;

function TElement.GetName(): string;
begin
  Result := Self.FName;
end;

function TElement.GetCount(): Integer;
begin
  Result := Self.FCount;
end;

procedure TElement.IncCount;
begin
  Inc(Self.FCount);
end;

function TElement.GetNextEl(): TElement;
begin
  Result := Self.FNextEl;
end;

procedure TElement.SetNextEl(ANextEl: TElement);
begin
  Self.FNextEl := ANextEl;
end;

function TElement.GetPrevEl(): TElement;
begin
  Result := Self.FPrevEl;
end;

procedure TElement.SetPrevEl(APrevEl: TElement);
begin
  Self.FPrevEl := APrevEl;
end;

end.
