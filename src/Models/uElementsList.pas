﻿unit uElementsList;

interface

uses
  SysUtils,
  uElement;

type
  TElementsList = class
  private
    FHead: TElement;
    FSize: Integer;
    procedure DeleteElement(AElement: TElement);
    function GetSize(): Integer;
    function GetHead(): TElement;
  public
    procedure AddElement(AName: string);
    procedure MakeListUnique();
    property Size: Integer read GetSize;
    property Head: TElement read GetHead;
  end;

implementation

procedure TElementsList.DeleteElement(AElement: TElement);
var
  NextElement, PrevElement: TElement;
begin
  NextElement := AElement.NextElement;
  PrevElement := AElement.PrevElement;

  if NextElement <> nil then
      NextElement.PrevElement := PrevElement;

  if PrevElement <> nil then
    PrevElement.NextElement := NextElement
  else
    Self.FHead := NextElement;

  Dec(Self.FSize);
  AElement.Destroy();
end;

procedure TElementsList.MakeListUnique;
var
  CurrElement, CmpElement, NextCmpElement: TElement;
begin
  CurrElement := Self.FHead;
  if CurrElement = nil then
    exit;

  CmpElement := CurrElement.NextElement;

  while CmpElement <> nil do
  begin
    NextCmpElement := CmpElement.NextElement;

    if CmpElement.Name = CurrElement.Name then
    begin
      Self.DeleteElement(CmpElement);
      CurrElement.IncCount();
    end;

    CmpElement := NextCmpElement;
    if CmpElement = nil then
    begin
      CurrElement := CurrElement.NextElement;
      if CurrElement <> nil then
        CmpElement := CurrElement.NextElement;
    end;
  end;
end;

function TElementsList.GetHead: TElement;
begin
  Result := Self.FHead;
end;

function TElementsList.GetSize(): Integer;
begin
  Result := Self.FSize;
end;

procedure TElementsList.AddElement(AName: string);
var
  ElementNode: TElement;
begin
  AName := Trim(AName);
  ElementNode := TElement.Create(AName);

  if Self.FHead = nil then
    Self.FHead := ElementNode
  else
  begin
    Self.FHead.PrevElement := ElementNode;
    ElementNode.NextElement := Self.FHead;
    Self.FHead := ElementNode;
  end;

  Inc(Self.FSize);
end;

end.
