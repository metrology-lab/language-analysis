unit uMainView;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.ExtDlgs,
  uLanguageAnalyzer,
  uElement;

type
  TfmMain = class(TForm)
    memProgramCode: TMemo;
    btnChooseFile: TButton;
    lblInfoText: TLabel;
    btnGo: TButton;
    lblProgramDictionary: TLabel;
    lblProgramLength: TLabel;
    lblProgramVolume: TLabel;
    lvOperandsList: TListView;
    lvOperatorsList: TListView;
    lblOperandsList: TLabel;
    lblOperatorsList: TLabel;
    pnlProgramDictionary: TPanel;
    pnlProgramLength: TPanel;
    pnlProgramVolume: TPanel;
    dlgOpen: TOpenTextFileDialog;
    pnlGeneralOperandsCount: TPanel;
    pnlGeneralOperatorsCount: TPanel;
    lblGeneralOperandsCount: TLabel;
    lblGeneralOperatorsCount: TLabel;
    procedure btnChooseFileClick(Sender: TObject);
    procedure btnGoClick(Sender: TObject);
  private
    FMetrics: TMetrics;
    procedure DrawMetrics;
  end;

var
  fmMain: TfmMain;

implementation

{$R *.dfm}

procedure TfmMain.btnChooseFileClick(Sender: TObject);
begin
  with Self.dlgOpen do
    if Execute then
      Self.memProgramCode.Lines.LoadFromFile(FileName);
end;

procedure TfmMain.btnGoClick(Sender: TObject);
var
  LanguageAnalyzer: TLanguageAnalyzer;
begin
  LanguageAnalyzer := TLanguageAnalyzer.Create();
  LanguageAnalyzer.ParseProgram(Self.memProgramCode.Text);
  Self.FMetrics := LanguageAnalyzer.Metrics;
  LanguageAnalyzer.Destroy();
  Self.DrawMetrics();
end;

procedure TfmMain.DrawMetrics;
var
  OperandIndex, OperatorIndex: Integer;
  CurrElement: TElement;
begin
  Self.lvOperandsList.Clear;
  Self.lvOperatorsList.Clear;

  Self.pnlProgramDictionary.Caption := IntToStr(Self.FMetrics.ProgramDictionary);
  Self.pnlProgramLength.Caption := IntToStr(Self.FMetrics.ProgramLength);
  Self.pnlProgramVolume.Caption := IntToStr(Self.FMetrics.ProgramVolume);
  Self.pnlGeneralOperandsCount.Caption := IntToStr(Self.FMetrics.GeneralOperandsCount);
  Self.pnlGeneralOperatorsCount.Caption := IntToStr(Self.FMetrics.GeneralOperatorsCount);

  CurrElement := Self.FMetrics.OperandsList.Head;
  OperandIndex := 1;
  while CurrElement <> nil do
  begin
    with Self.lvOperandsList.Items.Add do
    begin
      Caption := IntToStr(OperandIndex);
      SubItems.Add(CurrElement.Name);
      SubItems.Add(IntToStr(CurrElement.Count));
    end;
    CurrElement := CurrElement.NextElement;
    Inc(OperandIndex);
  end;

  CurrElement := Self.FMetrics.OperatorsList.Head;
  OperatorIndex := 1;
  while CurrElement <> nil do
  begin
    with Self.lvOperatorsList.Items.Add do
    begin
      Caption := IntToStr(OperatorIndex);
      SubItems.Add(CurrElement.Name);
      SubItems.Add(IntToStr(CurrElement.Count));
    end;
    CurrElement := CurrElement.NextElement;
    Inc (OperatorIndex);
  end;
end;
end.
