object fmMain: TfmMain
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1052#1077#1090#1088#1080#1082#1080' '#1061#1086#1083#1089#1090#1077#1076#1072
  ClientHeight = 778
  ClientWidth = 821
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 120
  TextHeight = 16
  object lblInfoText: TLabel
    Left = 16
    Top = 9
    Width = 155
    Height = 16
    Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1082#1086#1076' '#1087#1088#1086#1075#1088#1072#1084#1084#1099':'
  end
  object lblProgramDictionary: TLabel
    Left = 588
    Top = 208
    Width = 124
    Height = 16
    Caption = #1057#1083#1086#1074#1072#1088#1100' '#1087#1088#1086#1075#1088#1072#1084#1084#1099':'
  end
  object lblProgramLength: TLabel
    Left = 588
    Top = 264
    Width = 115
    Height = 16
    Caption = #1044#1083#1080#1085#1072' '#1087#1088#1086#1075#1088#1072#1084#1084#1099': '
  end
  object lblProgramVolume: TLabel
    Left = 588
    Top = 315
    Width = 112
    Height = 16
    Caption = #1054#1073#1098#1105#1084' '#1087#1088#1086#1075#1088#1072#1084#1084#1099':'
  end
  object lblOperandsList: TLabel
    Left = 16
    Top = 376
    Width = 113
    Height = 16
    Caption = #1057#1087#1080#1089#1086#1082' '#1086#1087#1077#1088#1072#1085#1076#1086#1074':'
  end
  object lblOperatorsList: TLabel
    Left = 416
    Top = 376
    Width = 119
    Height = 16
    Caption = #1057#1087#1080#1089#1086#1082' '#1086#1087#1077#1088#1072#1090#1086#1088#1086#1074':'
  end
  object lblGeneralOperandsCount: TLabel
    Left = 290
    Top = 376
    Width = 32
    Height = 16
    Caption = 'N2 = '
  end
  object lblGeneralOperatorsCount: TLabel
    Left = 680
    Top = 376
    Width = 32
    Height = 16
    Caption = 'N1 = '
  end
  object memProgramCode: TMemo
    Left = 16
    Top = 31
    Width = 537
    Height = 322
    ParentCustomHint = False
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btnChooseFile: TButton
    Left = 588
    Top = 31
    Width = 217
    Height = 50
    Caption = #1042#1099#1073#1088#1072#1090#1100' '#1092#1072#1081#1083'...'
    TabOrder = 1
    OnClick = btnChooseFileClick
  end
  object btnGo: TButton
    Left = 588
    Top = 105
    Width = 217
    Height = 48
    Caption = 'GO!'
    TabOrder = 2
    OnClick = btnGoClick
  end
  object lvOperandsList: TListView
    Left = 16
    Top = 408
    Width = 385
    Height = 353
    Columns = <
      item
        Caption = #8470
        MaxWidth = 50
        MinWidth = 50
      end
      item
        Caption = #1054#1087#1077#1088#1072#1085#1076
        MaxWidth = 285
        MinWidth = 285
        Width = 285
      end
      item
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        MaxWidth = 120
        MinWidth = 120
        Width = 120
      end>
    GridLines = True
    TabOrder = 3
    ViewStyle = vsReport
  end
  object lvOperatorsList: TListView
    Left = 420
    Top = 408
    Width = 385
    Height = 353
    Columns = <
      item
        Caption = #8470
        MaxWidth = 50
        MinWidth = 50
      end
      item
        Caption = #1054#1087#1077#1088#1072#1090#1086#1088
        MaxWidth = 285
        MinWidth = 285
        Width = 285
      end
      item
        Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        MaxWidth = 120
        MinWidth = 120
        Width = 120
      end>
    GridLines = True
    TabOrder = 4
    ViewStyle = vsReport
  end
  object pnlProgramDictionary: TPanel
    Left = 732
    Top = 204
    Width = 73
    Height = 24
    BorderStyle = bsSingle
    Color = clWhite
    ParentBackground = False
    TabOrder = 5
  end
  object pnlProgramLength: TPanel
    Left = 732
    Top = 260
    Width = 73
    Height = 24
    BorderStyle = bsSingle
    Color = clWhite
    ParentBackground = False
    TabOrder = 6
  end
  object pnlProgramVolume: TPanel
    Left = 732
    Top = 311
    Width = 73
    Height = 24
    BorderStyle = bsSingle
    Color = clWhite
    ParentBackground = False
    TabOrder = 7
  end
  object pnlGeneralOperandsCount: TPanel
    Left = 328
    Top = 372
    Width = 73
    Height = 24
    BorderStyle = bsSingle
    Color = clWhite
    ParentBackground = False
    TabOrder = 8
  end
  object pnlGeneralOperatorsCount: TPanel
    Left = 732
    Top = 372
    Width = 73
    Height = 24
    BorderStyle = bsSingle
    Color = clWhite
    ParentBackground = False
    TabOrder = 9
  end
  object dlgOpen: TOpenTextFileDialog
    Left = 564
    Top = 8
  end
end
