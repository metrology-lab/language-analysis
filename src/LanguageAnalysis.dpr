program LanguageAnalysis;

uses
  Vcl.Forms,
  uMainView in 'Views\uMainView.pas' {fmMain},
  uElement in 'Models\uElement.pas',
  uElementsList in 'Models\uElementsList.pas',
  uLanguageAnalyzer in 'Models\uLanguageAnalyzer.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfmMain, fmMain);
  Application.Run;
end.
